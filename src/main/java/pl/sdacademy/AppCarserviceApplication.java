package pl.sdacademy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppCarserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppCarserviceApplication.class, args);
    }

}
