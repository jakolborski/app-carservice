package pl.sdacademy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.sdacademy.car.Car;
import pl.sdacademy.car.CarRepository;
import pl.sdacademy.user.User;
import pl.sdacademy.user.UserRepository;

import java.time.LocalDate;

@Component
public class DbInit implements InitializingBean {
    private final static Logger LOGGER = LoggerFactory.getLogger(DbInit.class);

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private CarRepository carRepository;

    public DbInit(UserRepository userRepository, PasswordEncoder passwordEncoder, CarRepository carRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.carRepository = carRepository;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        LOGGER.info("Kod wykonywany po inicjalizacji beana");
        User user1 = new User("user1", passwordEncoder.encode("pass1"), "USER");
        userRepository.save(user1);
//        Car car1 = new Car(1,1, "BMW", "3");
        Car car2 = new Car();
        car2.setModel("e90");
        car2.setUserId(1);
        car2.setId(1);
        car2.setProducer("BMW");
        car2.setYear(2003);
        car2.setFuelType("Benzyna");
        car2.setOilInspectionDate(LocalDate.of(2020, 4, 1));
        car2.checkOilInspectionDate();
        car2.setMileage(200000);
        carRepository.save(car2);
    }
}
