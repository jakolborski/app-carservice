package pl.sdacademy.car;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.sdacademy.user.User;
import pl.sdacademy.user.UserService;

import java.util.List;
import java.util.Optional;

@Controller
//@RequestMapping("/car")
public class CarController {

    private CarRepository carRepository;
    private UserService userService;

    public CarController(CarRepository carRepository, UserService userService) {
        this.carRepository = carRepository;
        this.userService = userService;
    }

    @GetMapping("/cars")
    public String cars(ModelMap modelMap, Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        List<Car> lista = carRepository.findByUserId(user.getId());
        int overallCounter = 0;
        for (Car car : lista) {
            car.checkOilInspectionDate();
            car.checkServiceDate();
            car.checkInsuranceDate();
            overallCounter = overallCounter + car.howManyProblems();
        }
        modelMap.addAttribute("myList", lista);
        modelMap.addAttribute("counter", overallCounter);
        return "cars";
    }

    @GetMapping("/addcar")
    public String addCarGet(ModelMap modelMap) {
        modelMap.addAttribute("car", new Car());
        return "addcar-form";
    }
    //Z tego linku wzięte Authentication do pobierania userID
    //https://www.baeldung.com/get-user-in-spring-security

    @PostMapping("/addcar")
    public String addCarPost(ModelMap modelMap, @Validated Car car, BindingResult bindingResult, Authentication authentication) {
        modelMap.addAttribute("car", car);
        User user = (User) authentication.getPrincipal();
        if (bindingResult.hasErrors()) {
            return "addcar-form";
        } else {
            car.setUserId(user.getId());
            carRepository.save(car);
            return "addcar-result";
        }
    }

    @GetMapping("/cars/{carId}/edit")
    public String getEditInspecionPage(@PathVariable Integer carId, ModelMap modelMap, Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        Optional<Car> oCar = carRepository.findById(carId);
        if (oCar.isEmpty()) {
            return "no-access-car";
        } else {
            Car car = carRepository.findById(carId)
                    .orElse(null);
            if (car.getUserId().equals(user.getId())) {
                modelMap.addAttribute("car", car);
                return "edit-inspection";
            } else {
                return "no-access-car";
            }
        }
    }


    @PostMapping("/cars/{carId}/edit")
    public String editInsepctionPage(@PathVariable Integer carId, ModelMap modelMap, Car car) {
//        car = carRepository.findById(carId)
//                .orElse(null);
        modelMap.addAttribute("car", car);
        carRepository.save(car);
        return "edit-inspection-result";
    }

    @GetMapping("/cars/{carId}/delete")
    public String deleteCar(@PathVariable Integer carId, Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        Optional<Car> oCar = carRepository.findById(carId);
        if (oCar.isEmpty()) {
            return "no-access-car";
        } else {
            Car car = carRepository.findById(carId)
                    .orElse(null);
            if (car.getUserId().equals(user.getId())) {
                carRepository.deleteById(carId);
                return "deletecar-result";
            } else {
                return "no-access-car";
            }
        }
    }
}
