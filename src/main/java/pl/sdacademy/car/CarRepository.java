package pl.sdacademy.car;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface CarRepository extends JpaRepository<Car, Integer> {
    List<Car> findByUserId(Integer userId);

    void deleteById(@NotNull Integer id);

    @Modifying
    @Query("UPDATE Car SET oilType ='oilType' WHERE id =: id ")
    void updateOil(@Param("oilType") String oilType, @Param("id") Integer id);
}
