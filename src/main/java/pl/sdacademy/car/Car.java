package pl.sdacademy.car;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer userId;
    @Size(min = 2, message = "Producent musi mieć co najmniej {min} znaki")
    private String producer;
    @Size(min = 1, message = "Model musi mieć co najmniej {min} znak")
    private String model;
    @Range(min = 1900, max = 2021, message = "Rocznik musi być z zakresu od {min} do {max}")
    private int year;
    private String fuelType;
    //    @Size(min = 1, message = "Typ oleju musi posiadać znaki")
    private String oilType;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate oilInspectionDate;
    private boolean isOilInspectionSoon;
    //    @Positive(message = "Zasięg musi być dodatni")
    private int mileage;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate serviceDate;
    private boolean isServiceDateSoon;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate insuranceDate;
    private boolean isInsuranceDateSoon;
    private String otherNotes;


    public String producerAndModel() {
        return producer + " " + model;
    }

    public void checkOilInspectionDate() {
        if (getOilInspectionDate() != null) {
            if (ChronoUnit.DAYS.between(getOilInspectionDate(), LocalDate.now()) > 358 && getOilInspectionDate().isBefore(LocalDate.now())) {
                setOilInspectionSoon(true);
            }
        }
    }

    public void checkServiceDate() {
        if (getServiceDate() != null) {
            if (ChronoUnit.DAYS.between(getServiceDate(), LocalDate.now()) > 358 && getServiceDate().isBefore(LocalDate.now())) {
                setServiceDateSoon(true);
            }
        }
    }

    public void checkInsuranceDate() {
        if (getInsuranceDate() != null) {
            if (ChronoUnit.DAYS.between(getInsuranceDate(), LocalDate.now()) > 358 && getInsuranceDate().isBefore(LocalDate.now())) {
                setInsuranceDateSoon(true);
            }
        }
    }

    public int howManyProblems() {
        int counter = 0;
        if (isInsuranceDateSoon) {
            counter++;
        }
        if (isServiceDateSoon) {
            counter++;
        }
        if (isOilInspectionSoon) {
            counter++;
        }
        return counter;
    }
}
