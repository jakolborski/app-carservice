package pl.sdacademy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/login-error")
    public String loginFail() {
        return "login-error";
    }

    @GetMapping("/secured-stuff")
    public String securedStaff() {
        return "secured-stuff";
    }

    @GetMapping("/about-us")
    public String aboutUs() {
        return "about-us";
    }

    @GetMapping("/faq")
    public String FAQ() {
        return "faq";
    }

    @GetMapping("/logout")
    public String logout() {
        return "logout";
    }


}
